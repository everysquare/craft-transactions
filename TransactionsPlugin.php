<?php
namespace Craft;

class TransactionsPlugin extends BasePlugin
{
    public function init()
    {
    }

    function getName()
    {
        return Craft::t('Transactions');
    }

    function getVersion()
    {
        return '0.1';
    }

    function getDeveloper()
    {
        return 'EverySquare';
    }

    function getDeveloperUrl()
    {
        return 'http://everysquare.ca';
    }
}

<?php
namespace Craft;

class Transactions_TransactionsWidget extends BaseWidget
{
    public function getName()
    {
        return Craft::t('Transactions');
    }

    public function getBodyHtml()
    {
        return craft()->templates->render('transaction/widgets/transaction', array(
            'reportUrl' => craft()->config->get('reportUrl')
        ));
    }
}
